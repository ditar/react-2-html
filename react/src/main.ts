function initReactElement(reactClass:React.Component|any ,props:any, selector:string) { 
    const  e = React.createElement;  
    const domContainer = document.querySelector(selector);  
//    elts4React[selector] = ReactDOM.render(reactClass, domContainer);
    ReactDOM.render(e(reactClass, props), domContainer);
  
}